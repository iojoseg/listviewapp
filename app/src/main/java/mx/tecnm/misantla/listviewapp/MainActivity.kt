package mx.tecnm.misantla.listviewapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var frutas : ArrayList<String> = ArrayList()
        frutas.add("Limon")
        frutas.add("Manzana")
        frutas.add("Naranja")
        frutas.add("Uvas")
        frutas.add("kiwis")

        val lista = findViewById<ListView>(R.id.lista)

        val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,frutas)

        lista.adapter= adaptador

        lista.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            Toast.makeText(this,frutas.get(i),Toast.LENGTH_LONG).show()
        }



    }
}